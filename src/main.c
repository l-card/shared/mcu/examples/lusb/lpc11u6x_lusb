#include "chip.h"
#include "timer.h"

#ifdef USE_DEBUG_UART
#include "lprintf.h"
#else
#define lprintf(...)
#endif

#ifdef USE_LBOOT
    #include "crc.h"
    #include "lboot_req.h"
#endif

#include "lusb.h"



#include <string.h>

//#define USE_TEST_LED

#define TEST_LED_PIN LPC_PIN_PIO0_9_PIO

#define TEST_ARRAY_SIZE   16*1024


typedef enum {
    USB_CMD_PUT_ARRAY               = 1,
    USB_CMD_GET_ARRAY               = 2,
    USB_CMD_START                   = 3,
    USB_CMD_STOP                    = 4,
    USB_CMD_ENTER_LBOOT             = 5,
    USB_CMD_GET_MODULE_NAME         = 11,    
} t_usb_cmd_codes;


#define USB_CMD_VALUE_TEST_TX              0x01
#define USB_CMD_VALUE_TEST_RX              0x02


static const char f_devname[] = "lusb testdev";
static uint8_t f_test_array[TEST_ARRAY_SIZE];


#define ENABLE_TX 1
#define ENABLE_RX 1



#define TX_BUF_SIZE 512
#define TX_BUF_CNT  2
#define RX_BUF_SIZE 512
#define RX_BUF_CNT  2


#if ENABLE_TX
    static struct {
        uint32_t buf[TX_BUF_SIZE*TX_BUF_CNT];
        volatile uint8_t run;
        uint32_t cntr;
        uint8_t  buf_num;
    } f_tx_st;
#endif
#if ENABLE_RX
    static struct {
        uint32_t buf[RX_BUF_SIZE*RX_BUF_CNT];
        struct {
            uint32_t *buf;
            uint32_t size;
        } parts[RX_BUF_CNT];
        volatile uint8_t part_put_pos;
        uint8_t part_get_pos;
        volatile uint8_t run;
        uint8_t  buf_num;
        uint32_t cntr;
    } f_rx_st;
#endif

/* Признак, что есть работа по передаче данных для главного цикла */
static volatile uint8_t f_unproc_dd_work=0;





#if ENABLE_TX
static void f_stop_tx(void) {
    f_tx_st.run = 0;
    lusb_ep_clear(LUSB_STDBULK_TX_EP_NUM);
    lprintf("tx test stopped...\n");
}
#endif
#if ENABLE_RX
static void f_stop_rx(void) {
    f_rx_st.run = 0;
    lusb_ep_clear(LUSB_STDBULK_RX_EP_NUM);
    lprintf("rx test stopped...\n");
}
#endif






#ifdef USE_LBOOT


static t_lboot_params f_boot_params;
static int f_enter_lboot_req = 0;

__attribute__ ((section(".appl_info")))
const t_app_info g_app_info = { sizeof(t_app_info),
        LBOOT_APP_FLAGS_STABLE,       //flags
        "LK-30K"  //device name
        };

static t_lboot_devinfo f_info = {
    .devname = "LK-30K",
    .soft_ver = "0.1"

};

static void f_enter_lboot(void) {
    f_boot_params.hdr.flags = LBOOT_REQ_FLAGS_ENABLE_NO_SIGN | LBOOT_REQ_FLAGS_RECOVERY_WR;
    f_boot_params.hdr.size = LBOOT_REQ_SIZE(usb);
    f_boot_params.hdr.bootmode = LBOOT_BOOTMODE_USB;
    f_boot_params.hdr.devinfo = f_info;
    f_boot_params.hdr.timeout = 30000;
    f_boot_params.usb.flags = 0;
    f_boot_params.usb.crc = eval_crc16(0, (uint8_t*)&f_boot_params, f_boot_params.hdr.size-sizeof(f_boot_params.usb.crc));

    lusb_connect(0);
    __disable_irq();

    //устанавливаем стек и
    asm volatile (
            " .syntax unified\n"
            "movs r2, #4 \n\t"
            "movs r3, #1 \n\t"
            "skip_cfg: \n\t"
            "LDR  r1, [%1, #0] \n\t"
            "STR  r1, [%0, #0] \n\t"
            "ADD  %0, r2 \n\t"
            "ADD  %1, r2 \n\t"
            "SUBS %2, r3 \n\t"
            "CMP  %2, #0 \n\t"
            "BNE skip_cfg \n\t"
            "movs r2, #0 \n\t"
            "ldr r1, [r2] \n\t"
            "msr MSP, r1 \n\t"
            "ldr r1, [r2,#4] \n\t"
            "bx  r1 \n\t"
            " .syntax divided"
            : : "l" (LBOOT_REQ_ADDR), "l" (&f_boot_params), "l" ((f_boot_params.hdr.size+3)/4)
            : "r1", "r2", "r3" );
}
#endif



int main(void) {

    /* настройка ножки светодиода, если нужно */
    LPC_PIN_CONFIG(TEST_LED_PIN, 0);
    LPC_PIN_DIR_OUT(TEST_LED_PIN);

    LPC_PIN_CONFIG(TEST_LED_PIN, 0);


    LPC_PIN_CONFIG(LPC_PIN_PIO0_8_PIO, 0);
    LPC_PIN_CONFIG(LPC_PIN_PIO0_10_PIO, 0);
    LPC_PIN_CONFIG(LPC_PIN_PIO0_18_PIO, 0);
    LPC_PIN_CONFIG(LPC_PIN_PIO0_19_PIO, 0);

    LPC_PIN_CONFIG(LPC_PIN_PIO0_21_PIO, 0);
    LPC_PIN_CONFIG(LPC_PIN_PIO0_22_PIO, 0);
    LPC_PIN_CONFIG(LPC_PIN_PIO0_23_PIO, 0);
    LPC_PIN_CONFIG(LPC_PIN_PIO1_20_PIO, 0);
    LPC_PIN_CONFIG(LPC_PIN_PIO1_21_PIO, 0);
    LPC_PIN_CONFIG(LPC_PIN_PIO1_23_PIO, 0);

    LPC_PIN_CONFIG(LPC_PIN_PIO0_1_PIO, 0);
    LPC_PIN_DIR_OUT(TEST_LED_PIN);

    LPC_CLK_DIS(SYSCTL_CLOCK_SSP0);






#ifdef USE_TEST_LED
    LPC_PIN_OUT(TEST_LED_PIN, 1);

    /* таймер используются в данном примере только для
     * морагания раз в секунду светодиодом */
    clock_init();
    t_timer tmr;
    timer_set(&tmr, CLOCK_CONF_SECOND/2);
#else
    LPC_PIN_OUT(TEST_LED_PIN, 0);
#endif

#ifdef USE_LBOOT
    NVIC_EnableIRQ(BOD_WDT_IRQn);
#endif


#ifdef USE_DEBUG_UART
    lprintf_uart_init(0, 115200, 8, LPRINTF_UART_PARITY_NONE, LPC_PIN_PIO0_19_U0_TXD, 0);
    lprintf("start. sysclk = %d\n", LPC_SYSCLK);
#endif

    /* инициализация стека USB */
    lusb_init();
    /* подключение устройства к шине USB */
    lusb_connect(1);

    lprintf("lusb init done!\n");

    for (;;) {
#ifdef USE_LBOOT
        lpc_wdt_reset();
#endif
        /* выполнение фоновых задач USB стека. Должна вызваться переодически
         * из основного цикла. Не может вызываться из callback-функций */
        lusb_progress();
#ifdef USE_TEST_LED
        if (timer_expired(&tmr)) {
            LPC_PIN_TOGGLE(TEST_LED_PIN);
            timer_reset(&tmr);
        }
#endif

#ifdef USE_LBOOT
        if (!lusb_has_background_work() && f_enter_lboot_req) {
            f_enter_lboot();
        }
#endif

        /* если нет фоновых задач USB и не было прерываний, которые
         * могли бы потребовать обработку внутри main(), то можем остановить
         * ядро до следующего прерывания */
        if (!f_unproc_dd_work && !lusb_has_background_work()) {
            __irq_disable();
            /* необходимо выполнить повторную проверку с запрещенными
             * прерываниями, так как после первой проверки и до запрещения
             * прерываний прерывание могло произойти и результат проверки измениться */
            if (!f_unproc_dd_work && !lusb_has_background_work()
#ifdef USE_LBOOT
                    && !f_enter_lboot_req
#endif
                    ) {
                __wfi();               
            }
            __irq_enable();
        }


        if (f_unproc_dd_work) {
            /* сбрасываем признак перед выполнением работы по передаче,
             * если признак установится во время обработки и успеем выполнить
             * эту работу в этом цикле, то это приведет лишь к одному лишнему
             * проходу, но сам признак всегда будет обработан */
            f_unproc_dd_work = 0;
#if ENABLE_TX
            if (f_tx_st.run) {
                /* если заданий на передачу меньше, чем кол-во буферов - пробуем
                 * добавить новое задание */
                int dd_in_progr = lusb_ep_get_dd_in_progress(LUSB_STDBULK_TX_EP_NUM);
                int res = 0;
                while (((dd_in_progr >= 0) && (dd_in_progr < TX_BUF_CNT)) && (res==0)) {
                    uint32_t *tx_buf = &f_tx_st.buf[TX_BUF_SIZE*f_tx_st.buf_num];
                    unsigned cntr = f_tx_st.cntr;
                    /* заполняем тестовым счетчиком буфер*/
                    for (unsigned i=0; i < TX_BUF_SIZE; i++) {
                        tx_buf[i] = cntr++;
                    }

                    /* ставим задание на передачу */
                    res = lusb_ep_add_dd(LUSB_STDBULK_TX_EP_NUM, tx_buf,
                                             TX_BUF_SIZE*sizeof(tx_buf[0]), 0);

                    if (res==LUSB_ERR_SUCCESS) {
                        /* переходим к следующему буферу при успехе */
                        if (++f_tx_st.buf_num==TX_BUF_CNT)
                            f_tx_st.buf_num = 0;
                        f_tx_st.cntr = cntr;
                        dd_in_progr++;
                    } else {                        
                        lprintf("tx add dd error: %d\n", res);
                        f_unproc_dd_work = 1;
                    }
                }
            }
#endif
#if ENABLE_RX
            if (f_rx_st.run) {
                int res = 0;
                int dd_in_progr = lusb_ep_get_dd_in_progress(LUSB_STDBULK_RX_EP_NUM);

                /* обработка принятых блоков данных - проверка счетчика */
                while (f_rx_st.part_get_pos != f_rx_st.part_put_pos) {
                    uint32_t size = f_rx_st.parts[f_rx_st.part_get_pos].size/sizeof(f_rx_st.buf[0]);
                    uint32_t *buf = f_rx_st.parts[f_rx_st.part_get_pos].buf;
                    uint32_t i;
                    uint32_t cntr = f_rx_st.cntr;

                    for (i=0; i < size; i++) {
                        if (buf[i] != cntr)  {
                            lprintf("rx err: expect 0x%08X, recvd 0x%08X\n",
                                    cntr, buf[i]);
                            cntr = buf[i];
                        }
                        cntr++;
                    }

                    if (++f_rx_st.part_get_pos == sizeof(f_rx_st.parts)/sizeof(f_rx_st.parts[0])) {
                        f_rx_st.part_get_pos = 0;
                    }

                    f_rx_st.cntr = cntr;
                }

                /* постановка новых задач на прием, если задач меньше, чем кол-во буферов.
                 * dd_in_progr получаем до обработки принятых блоков, чтобы не было ситуации
                 * когда новый запрос будет завершен при обработке блока и
                 * мы поставим прием на ту часть буфера, которую еще не обработали */
                while (((dd_in_progr >= 0) && (dd_in_progr < RX_BUF_CNT)) && (res==0)) {
                    res = lusb_ep_add_dd(LUSB_STDBULK_RX_EP_NUM,
                                             &f_rx_st.buf[RX_BUF_SIZE*f_rx_st.buf_num],
                                             RX_BUF_SIZE*sizeof(f_rx_st.buf[0]), 0);

                    if (res==LUSB_ERR_SUCCESS) {                        
                        if (++f_rx_st.buf_num==RX_BUF_CNT)
                            f_rx_st.buf_num = 0;
                        dd_in_progr++;
                    } else {
                        lprintf("rx add dd error: %d\n", res);
                        f_unproc_dd_work = 1;
                    }
                }
#endif
            }
        }
    }

    return 0;
}


/* Функция обработки управляющих запросов по нулевой конечной точке с данными
 * от хоста в устройства (или без данных).
 * req содержит все параметры запроса, а buf - данные (длиной (req->length)) */
int lusb_appl_cb_custom_ctrlreq_rx(const t_lusb_req* req, uint8_t* buf) {
    int res = LUSB_ERR_UNSUPPORTED_REQ;
    if ((req->req_type & USB_REQ_REQTYPE_TYPE) == USB_REQ_REQTYPE_TYPE_VENDOR) {
        switch (req->request) {
#ifdef USE_LBOOT
            case USB_CMD_ENTER_LBOOT : {
                    f_enter_lboot_req = 1;
                    lprintf("bootloader request!\n");
                    res = LUSB_ERR_SUCCESS;
                }
                break;
#endif
            case USB_CMD_PUT_ARRAY: {
                    unsigned addr = ((uint32_t)req->index << 16) | req->val;
                    int len = req->length;
                    if ((addr + len) <= TEST_ARRAY_SIZE) {
                        memcpy(&f_test_array[addr], buf, len);
                        res = LUSB_ERR_SUCCESS;
                    }
                }
                break;
            case USB_CMD_START: {
#ifdef ENABLE_TX
                    if (req->val & USB_CMD_VALUE_TEST_TX) {
                        if (f_tx_st.run) {
                            f_stop_tx();
                        }
                        f_tx_st.cntr = 0;
                        f_tx_st.buf_num = 0;
                        f_tx_st.run = 1;
                        lprintf("tx test started!\n");
                    }
#endif
#ifdef ENABLE_RX
                    if (req->val & USB_CMD_VALUE_TEST_RX) {
                        if (f_rx_st.run) {
                            f_stop_rx();
                        }
                        f_rx_st.part_get_pos = f_rx_st.part_put_pos = 0;
                        f_rx_st.cntr = 0;
                        f_rx_st.buf_num = 0;
                        f_rx_st.run = 1;
                        lprintf("rx test started!\n");
                    }
#endif
                    f_unproc_dd_work = 1;
                    res = LUSB_ERR_SUCCESS;
                }
                break;
            case USB_CMD_STOP: {
#ifdef ENABLE_TX
                    if (req->val & USB_CMD_VALUE_TEST_TX) {
                        f_stop_tx();
                    }
#endif
#ifdef ENABLE_RX
                    if (req->val & USB_CMD_VALUE_TEST_RX) {
                        f_stop_rx();
                    }
#endif
                    res = LUSB_ERR_SUCCESS;
                }
                break;
        }
    }
    return res;
}

/* Функция вызывается при обработке управляющего запроса с данными от устройства к хосту.
 * Если запрос обработан, фунция должна вернуть указатель на буфер c
 * данными для передачи и размер данных в переменной length.
 * Функция может использовать стандартный буфер размером LUSB_EP0_BUF_SIZE байт,
 * указатель на который передан в tx_buf.
 * Если используется свой буфер с данными, то он не должен изменяться до
 * завершения передачи управляющего запроса (когда lusb_has_background_work() или
 * начнется следующий запрос).
 * Если запрос не поддерживается - возвращается 0-ой указатель.
 */
const void* lusb_appl_cb_custom_ctrlreq_tx(const t_lusb_req *req, int *length, uint8_t* tx_buf) {
    const void* wr_buf = NULL;
    if ((req->req_type & USB_REQ_REQTYPE_TYPE) == USB_REQ_REQTYPE_TYPE_VENDOR) {
        switch (req->request) {
            case USB_CMD_GET_MODULE_NAME:
                *length = sizeof(f_devname);
                wr_buf = f_devname;
                break;
            case USB_CMD_GET_ARRAY: {
                    unsigned addr = ((uint32_t)req->index << 16) | req->val;
                    int len = req->length;
                    if (addr < TEST_ARRAY_SIZE) {
                        if ((addr + len) > TEST_ARRAY_SIZE) {
                            len = TEST_ARRAY_SIZE - addr;
                        }
                        *length = len;
                        wr_buf = &f_test_array[addr];
                    }
                }
                break;
        }
    }
    return wr_buf;
}


/* Функция вызывается при возникновении событий по конечной точке.
 * Сброс останова (LUSB_EP_EVENT_HALT_CLR) является признаком сброса кт, поэтому
 * используем его также как признак останова обмена. Также останавливаем обмен,
 * если эта кт была запрещена */
void lusb_appl_cb_ep_event(uint8_t iface, uint8_t ep_idx, t_lusb_ep_event event) {
    if ((event == LUSB_EP_EVENT_DISABLED) || (event == LUSB_EP_EVENT_HALT_CLR)) {
#ifdef ENABLE_TX
        if (ep_idx == LUSB_STDBULK_TX_EP_NUM) {
            f_stop_tx();
        }
#endif
#ifdef ENABLE_RX
        if (ep_idx == LUSB_STDBULK_RX_EP_NUM) {
            f_stop_rx();
        }
#endif
    }
}


/* Функция вызывается при смене состояния устройства.
 * Может вызываться из прерывания */
void lusb_appl_cb_devstate_ch(uint8_t old_state, uint8_t new_state) {
    if (new_state & LUSB_DEVSTATE_SUSPENDED) {
        lprintf("enter suspend\n");
    } else if (old_state & LUSB_DEVSTATE_SUSPENDED) {
        lprintf("leave suspend\n");
    }
}

/* Функция вызывается при завершении (или другому событию) обмена,
 *  добавленного через lusb_ep_add_dd(), по одной из конечных точек.
 * Функция может вызываться из прерывания.
 * Если обмен завершен (event == LUSB_DD_EVENT_EOT), структура pDD содержит
 *  информацию о завершенном обмене */
void lusb_appl_cb_dd_event(uint8_t iface, uint8_t ep_idx, t_lusb_dd_event event, const t_lusb_dd_status* pDD) {
    if (event == LUSB_DD_EVENT_EOT) {
#ifdef ENABLE_RX
        /* при приема обмен может завершится по факту приема пакета длины, не кратной
         * размеру пакета для конечной точки. Поэтому для приема нужно сохранять
         * реальный размер принятого пакета и использовать его, а не размер
         * при добавлении задания на передачу через lusb_ep_add_dd() */
        if (ep_idx==LUSB_STDBULK_RX_EP_NUM) {
            if ((pDD->status == LUSB_DDSTATUS_CPL_SUCCESS) || (pDD->status == LUSB_DDSTATUS_CPL_UNDERRUN)) {
                f_rx_st.parts[f_rx_st.part_put_pos].buf = (uint32_t*)(pDD->last_addr - pDD->trans_cnt);
                f_rx_st.parts[f_rx_st.part_put_pos].size = pDD->trans_cnt;
                if (++f_rx_st.part_put_pos == sizeof(f_rx_st.parts)/sizeof(f_rx_st.parts[0])) {
                    f_rx_st.part_put_pos = 0;
                }
            }
        }
#endif
        /* ставим флаг, что было прерывание, и в основном цикле может появилась
         * фоновая работа по добавлению новых передач и/или проверки новых, т.е.
         * засыпать пока нельзя */
        f_unproc_dd_work=1;
    }
}

#ifdef LUSB_USE_MAN_GEN_SERIAL
    const char* lusb_app_cb_get_serial(void) {
        return "1234";
    }
#endif

void __attribute__ ((__interrupt__)) BOD_WDT_IRQHandler(void) {
    LPC_WWDT->MOD =  WWDT_WDMOD_WDINT;
}
