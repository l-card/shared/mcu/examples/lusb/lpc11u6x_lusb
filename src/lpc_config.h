#ifndef LPC_CONFIG_H
#define LPC_CONFIG_H


#define LPC_CLKF_OSC        16000000UL             /* Частота кварца OSC, Гц; 0 - не используется */

#define LPC_MAINCLK_SRC     LPC_MAINCLK_SRC_PLLOUT /* источник главного клока */
#define LPC_SYSCLK_DIV      8   /* 1..255.  Fcpu = main clock / LPC_SYSCLK_DIV */

/****              System PLL (Fout = Fin * M, Fcco = Fout*2*P)    ******/
#define LPC_USE_SYSPLL      1               /* Включение системного PLL */
#define LPC_SYSPLL_SRC      LPC_SYSPLL_SRC_SYSOSC
#define LPC_SYSPLL_M        4              /* M = 1..32  */
#define LPC_SYSPLL_P        2              /* P = 1,2,4,8 */

/****                USB              ******/
#define LPC_USE_USBCLK      1
#define LPC_USBCLK_SRC      LPC_USBCLK_SRC_USBPLL
#define LPC_USBCLK_DIV      1
/* Настройки PLL для USB (Fout = Fin * M, Fcco = Fout*2*P).
 * Должны быть действительные даже если используем main clock для USB */
#define LPC_USBPLL_SRC      LPC_USBPLL_SRC_SYSOSC
#define LPC_USBPLL_M        3  /* M = 1..32  */
#define LPC_USBPLL_P        2  /* P = 1,2,4,8 */


/****                WDT OSC          *******/
#define LPC_USE_WDTOSC      0
#define LPC_WDT_FREQSEL     LPC_WDT_FREQSEL_4_60
#define LPC_WDT_DIVSEL      1 /* 0..31.  Fwdtosc = Fana / (2 * (1+LPC_WDT_DIVSEL)) */

/* использование ножки CLKOUT для генерации выходной частоты */
#define LPC_USE_CLKOUT      0
#define LPC_CLKOUT_SRC      LPC_CLKOUT_SRC_MAINCLK
#define LPC_CLKOUT_DIV      4

/* Если не 0, то по завершению инициализации нужно отключить IRC осциллятор */
#define LPC_IRC_PD          1

/* Делители частоты для переферии (0 - клок отключен) */
#define LPC_USART0_CLK_DIV  1
#define LPC_SSP0_CLK_DIV    0
#define LPC_SSP1_CLK_DIV    0
#define LPC_FRG_CLK_DIV     0 /* делитель входной частоты для Fractional Divider для USART1-4 */

/* Настройки Fractional Divider для USART1-4. Имеют значения только если
 * LPC_FRG_CLK_DIV != 0.
 * Fuart = main clock / (LPC_FRG_CLK_DIV * (1 + FRG_MUL/FRG_DIV)) */
#define LPC_USART_FRG_DIV   256
#define LPC_USART_FRG_MUL   1

#define LPC_INIT_WDT 0

#endif // LPC_CONFIG_H
