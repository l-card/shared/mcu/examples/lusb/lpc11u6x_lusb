PROJECT_NAME := lpc11u_lusb
TARGET_CPU := lpc11u6x


OPT := -O2
USE_DEBUG_UART := 1
#USE_LBOOT := 1


include ./lib/chip/chip.mk
include ./lib/lprintf/lprintf_src.mk
include ./lib/lusb/lusb_src.mk


BUILD_GROUPS = STD USB_DESCR

LDSCRIPT_TEMPLATE := ./lpc11U68.ld.S


DEFS := NDEBUG STACK_SIZE=1024 LBOOT_USE_USB $(CHIP_DEFS)


SRC_STD  :=  $(CHIP_SRC) \
       $(CHIP_STARTUP_SRC) \
        $(LUSB_SRC) \
       ./src/main.c \
       ./lib/timer/ports/cm4_systick/clock.c
       
SRC_USB_DESCR := $(LUSB_SRC_DESCR)

CFLAGS = $(CHIP_CFLAGS)  $(OPT) -std=gnu99
CFLAGS_USB_DESCR = $(LUSB_SRC_DESCR_CPFLAGS)       
LDFLAGS  = $(CHIP_LDFLAGS) -nostartfiles

SYS_INC_DIRS := $(CHIP_INC_DIRS)

INC_DIRS :=  ./src \
             $(LUSB_INC_DIRS) \
             ./lib/timer \
             ./lib/timer/ports/cm4_systick \
             ./lib/lcspec \
             ./lib/lcspec/gcc

             
ifdef USE_DEBUG_UART
    DEFS += USE_DEBUG_UART
    SRC_STD += $(LPRINTF_SRC)
    INC_DIRS += $(LPRINTF_INC_DIRS)
endif

ifdef USE_LBOOT
    DEFS += FLASH_START_ADDR=0x00008100
    DEFS += USE_LBOOT
    INC_DIRS += ./lib/crc
    SRC_STD += ./lib/crc/crc.c
else
    DEFS += FLASH_START_ADDR=0
endif

LDDEFS = $(DEFS)

include ./lib/gnumake_prj/gcc_prj.mk

$(PROJECT_TARGET): $(PROJECT_BIN) $(PROJECT_HEX)

